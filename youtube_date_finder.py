import requests


while True:
    try:
        print('Enter YouTube URL:')
        url = input()
        r = requests.get(url).text
        publish_date_index = r.find('publishDate')
        if publish_date_index > 0:
            print('Video was uploaded at:', end=' ')
            for i in range(10):
                print(r[publish_date_index + 14], end='')
                publish_date_index += 1
            print()
        else:
            print('Video not found')
        
    except Exception as e:
        print(e)

